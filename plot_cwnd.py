import sys
from siftr import *

if __name__ == '__main__':

    args = sys.argv[1:]

    s = SIFTRFile(args[0])
    # s.completion_time(dst_port=5001, src="10.0.1.7", dst="10.0.0.5")
    # s.completion_time(dst_port=5002, src="10.0.1.7", dst="10.0.0.5")

    p = Plot()
    p.title("CWND")
    p.labels("Time (s)", "bytes")

    x,y = s.cwnd(dst_port=5001, src="10.0.1.7", dst="10.0.0.5")
    p.plot(x,y, "cwnd 1")

    # for i in range(0, len(x)):
    #     print(x[i],y[i])

    x,y = s.ssthresh(dst_port=5001, src="10.0.1.7", dst="10.0.0.5")
    p.plot(x,y, "ssthresh")

    x,y = s.cwnd(dst_port=5002, src="10.0.1.7", dst="10.0.0.5", time_offset=5.0)
    p.plot(x,y, "cwnd 2")

    p.save(args[1])

