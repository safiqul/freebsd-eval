#! /usr/bin/env bash

INFILE=$1

xzcat $INFILE | grep ",5001," | sed 's/,/ /g' > cwnd_5001
xzcat $INFILE | grep ",5002," | sed 's/,/ /g' > cwnd_5002

START=`head -1 cwnd_5001 | cut --delim=" " -f 3`

gnuplot -persist <<EOF

set terminal png
set output "cwnds.png"

set yrange [0:300000]

set title "$INFILE"

plot "cwnd_5001" using (\$3 - $START):9 with lines, "cwnd_5002" using (\$3 - $START):9 with lines

EOF
