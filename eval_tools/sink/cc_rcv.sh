#!/bin/sh

DURATION=$1
FLOWS=$2
PORT_A=5001
PORT_B=5002

if [ -z $DURATION ]; then #  || [ $FLOWS -lt 1 ]; then
    echo "Must set duration" #  and flows!"
    exit 1
fi

#Kill any residual iperf servers
pkill -9 iperf


iperf -s -p $PORT_A &
iperf -s -p $PORT_B &


sleep $DURATION
#iperf daemon mode doesn't react to SIGTERM for some reason:
pkill -9 iperf
