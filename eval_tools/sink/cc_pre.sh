#!/bin/sh

TIU=0
FSE=0
SIFTR=0

echo "Executing $0"

for arg; do
    if [ "$arg" == "tiu" ]; then
	TIU=1
    fi
    if [ "$arg" == "fse" ]; then
	FSE=1
    fi
    if [ "$arg" == "siftr" ]; then
	SIFTR=1
    fi
done

if [ $SIFTR -eq 1 ]; then
    kldload siftr
fi

# TIU_MASTER=`sysctl -n net.inet.tcp.hijack.enabled`

# Enabling, disabling then reenabling TiU crashes -> workaround:
# if [ "$TIU_MASTER" -eq 1 ]; then
#     if [ "$TIU" -eq 0 ]; then
# 	sysctl net.inet.tcp.hijack.default_disabled=1
#     else
# 	sysctl net.inet.tcp.hijack.default_disabled=0
#     fi
# else
#     if [ "$TIU" -eq 1 ]; then
# 	sysctl net.inet.tcp.hijack.enabled=1
#     fi
# fi

if [ "$FSE" -eq 1 ]; then
    sysctl net.inet.tcp.cc.algorithm=newreno_afse
else
    sysctl net.inet.tcp.cc.algorithm=newreno
fi

if [ "$SIFTR" -eq 1 ]; then
    rm /var/log/siftr.log    
    sysctl net.inet.siftr.enabled=1
fi
echo "PRE request: TiU=$TIU FSE=$FSE SIFTR=$SIFTR"

echo "$0 finished"
