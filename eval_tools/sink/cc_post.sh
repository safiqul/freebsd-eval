#!/bin/sh

# If we want to clean up SIFTR logging, first arg must be the intended logname!
SIFTR=0

for arg; do
    if [ $arg == 'siftr' ]; then
	SIFTR=1
	LOGNAME=$1
    fi
done

if [ $SIFTR == 1 ]; then
    sysctl net.inet.siftr.enabled=0

    cd /var/log
    sync
    mv siftr.log siftr-$LOGNAME.log
    xz siftr-$LOGNAME.log

    echo "Swept up after SIFTR; log is siftr-$LOGNAME.log.xz"
fi

sysctl net.inet.tcp.cc.algorithm=newreno
