#!/bin/bash
DEV=$1
OUTFILE=$2
DURATION=$3
if [[ ! -z $DURATION ]]; then
    UNTIL=`expr \`date +%s\` + $DURATION`
fi

while true
do
    if [[ ! -z $DURATION ]]; then
	CONT=`expr $UNTIL - \`date +%s\` \> 0`
	if [[ $CONT -eq 0 ]]; then
	    break;
	fi
    fi

    QSTAT=`tc -s qdisc show dev $DEV` # >out.dat
    #cat out.dat |awk '/backlog/{print $0}'|cut -d' ' -f4|cut -d 'p' -f1 #>>qlen.txt
    QLEN=`echo "$QSTAT" |awk '/backlog/{print $0}'|cut -d' ' -f4|cut -d 'p' -f1`
    DROPPED=`echo "$QSTAT" |awk '/dropped/{print $0}'|cut -d' ' -f8|cut -d ',' -f1`
    if [[ ! -z $OUTFILE ]]; then
	echo "$QLEN $DROPPED" `date -Ins` >> $OUTFILE
    else
	echo "$QLEN $DROPPED" `date -Ins`
    fi
    sleep 0.05
done
#cat out.dat |awk '/backlog/{print $0}'|cut -d' ' -f4|cut -d 'p' -f1 >qlen.txt
#rm out.dat

