from experiment import ExpRunner, LocalSetupExperiment
import time

class TransferTimeExpRunner(ExpRunner):
    def __init__(self):
        fdur = 30 #250 for 20M/2M
        maxflows = 1
        expname = "cc"
        configs = {
            'regular': {'fse': False}
            # 'fse': {'fse': True}
        }
        replicates = 1 #10
        self.run_off = 0 # Offset from run 1 to start at
        # self.rate_configs = [2e6, 4e6, 8e6, 10e6]
        self.rate_configs = [10e6]
        self.delay = 100.0

        ExpRunner.__init__(self, expname, LocalSetupExperiment, configs, \
                           replicates, fdur, xtraffic=False)
        self.flowconfigs = range(1, maxflows + 1) #Overriding default definition
        self.tracetypes.append("iperf")

        self.config_data['rate_configs'] = self.rate_configs
        self.config_data['rtt'] = self.delay
        self.config_data['comment'] = 'nflows represents RTT'

        self.if_pcap = "veth8.1."
        self.if_qlen = "veth11.0."

        self.expid = time.strftime("%Y-%m-%dT%H%M")

    def instantiate_exp(self, rate, config):
        return self.expclass(self.expname, self.expid, \
                             rate, duration=self.fdur, tiu=False, \
                             fse=self.configs[config]['fse'], \
                             iface_pcap=self.if_pcap + self.if_suffix, \
                             iface_qlen=self.if_qlen + self.if_suffix, rate=rate, \
                             delay=self.delay)

    def _run_inner(self):
        for config in self.configs:
            for rate in self.rate_configs:
                e = self.instantiate_exp(rate, config)
                run = 1 + self.run_off
                for replicate in range(self.replicates):
                    res = e.run(config + ("/run%03d" % run))

                    self.report.append({'config': config, 'nflows':
                                   rate, 'expid': e.expid, \
                                   'result': res, 'run': run,
                                   'duration': e.duration})

                    run += 1


if __name__ == '__main__':
    ttcexp = TransferTimeExpRunner()
    ttcexp.run()
