from subprocess32 import Popen, PIPE, TimeoutExpired, check_output
import threading
from threading import Thread
import signal
import os
import os.path
import time
import shlex
import errno
import tarfile
import csv
import json
import sys
from Queue import Queue, Empty

SRC = "10.0.1.7"
DST = "10.0.0.5"


def get_output_dir(root, exp, perf):
    return os.path.join(root, exp.exp + "-" + exp.expid, perf)

def ensure_output_dir(root, exp, perf):
    path = get_output_dir(root, exp, perf)
    if not os.path.exists(path):
        print("Creating ", path) 
        os.makedirs(path)
    return path

def get_output_file(root, exp, perf, ext):
    path = ensure_output_dir(root, exp, perf)
    f = os.path.join(path, exp.runid.replace("/", "-") + "." + ext) if ext is not None else os.path.join(path, exp.runid.replace("/", "-"))
    return f

def enqueue_output(out, prefix, queue):
    if prefix:
        for line in iter(out.readline, b''):
            queue.put(prefix + line)
    else:
        for line in iter(out.readline, b''):
            queue.put(line)

    out.close()

def install_script(script_name, local_dir, host, remote_dir):
    scp_cmd = "scp %s/%s %s:%s/%s" % (local_dir, script_name, host, remote_dir, script_name)
    print(scp_cmd)

    try:
        check_output(shlex.split(scp_cmd))
    except:
        print "Failed to install script on remote machine"
        return 1

def timeleft(until):
    dt = until - time.time()
    return dt if dt >= 0 else 0.0

def report_print(report):
    for exp in report:
        print "%s\t%s\t%03d\t%s\t%6.2f\t\t%2d" % (exp['config'], exp['expid'], \
                                          exp['run'], "%6d" % exp['nflows'] \
                                                 if type(exp['nflows']) == int \
                                                 else str(exp['nflows']), \
                                          exp['duration'], exp['result'])

def find_dev_ext():
    tc_cmd = "tc qdisc show | grep tbf | head -n 1 | cut -d ' ' -f 5"
    dev = check_output(tc_cmd, shell=True)
    return dev.split(".")[-1].strip()

def find_core_session():
    pycore_cmd = "find /tmp/ -type d -name 'pycore.*' | cut -d '.' -f 2"
    core_session = check_output(pycore_cmd, shell=True).strip()
    return core_session

def start_crosstraffic(core_session, runno):
    #Replay a trace of 345s heavy tail traffic (out of 10 available traces)
    #Need to rewrite MAC addrs because it was captured on the bottleneck link
    tcpr_cmd = "tcpreplay-edit --enet-dmac=00:00:00:aa:00:04 --enet-smac=00:00:00:aa:00:03" \
               " --mtu-trunc --intf1=eth0 /home/kah/crosstraffic/xt%02d.pcapng" % runno
    src_cmd = "vcmd -c /tmp/pycore.%s/n7 -- %s" % (core_session, tcpr_cmd)
    #With the replay, we don't care what happens on the other end
    #sink_cmd = "vcmd -c /tmp/pycore.%s/n5 -- ITGRecv" % core_session
    print src_cmd

    #D-ITG likes to spam the console, silence it: (same for tcpreplay)
    stfu = open(os.devnull, 'w')

    #sink = Popen(shlex.split(sink_cmd), stdout=stfu)
    #time.sleep(.5)
    source = Popen(shlex.split(src_cmd), stdout=stfu)
    time.sleep(.5)
    print "\n\n\n"

    return source #, sink

def compress_file(filename):
    try:
        xz_cmd = "xz -2 %s" % filename
        check_output(xz_cmd, shell=True)
    except CalledProcessError as e:
        print "Compression of file '%s' failed: %s" % (filename, e)


class Experiment(object):
    iface_pcap = "veth8.1.30"
    iface_qlen = "veth11.0.30"
    exp = "fairness"
    #runid = "regular/run20"
    flows = 2
    duration = 10
    fuzz = 20

    def __init__(self, name, expid, flows=1, duration=10, \
                 iface_pcap=None, iface_qlen=None, fuzz=None):
        self.exp = name
        #self.runid = runid
        self.flows = flows
        self.duration = duration
        self.iface_pcap = iface_pcap if iface_pcap else self.iface_pcap
        self.iface_qlen = iface_qlen if iface_qlen else self.iface_qlen
        self.fuzz = fuzz if fuzz else self.fuzz
        # self.expid = time.strftime("%Y-%m-%dT%H%M")
        self.expid = expid
    
    def sigint_handler(self, *args):
        self.agent_killer()

    def agent_killer(self):
        agents = self.agents
        for p in agents:
            try:
                if agents[p].poll() is None:
                    agents[p].terminate()
            except OSError:
                print "Couldn't TERM %s" % p

    def stop_qlen(self, qlen):
        tries = 0
        try:
            while tries < 3 and qlen.poll() is None:
                qlen.terminate()
                tries += 1
                time.sleep(0.01)
            if qlen.poll() is None:
                qlen.kill()
        except OSError:
            pass #lol

    def run_pre(self):
        return 0

    def run_main(self):
        print "=====\nExperiment %s-%s Run %s STARTING\n=====" \
            % (self.exp, self.expid, self.runid)

        self.pcap_file = get_output_file("/home/kah/cc/traces", self, "pcap", "pcap")
        pcap_cmd = "tshark -i %s -F pcap -w %s -n -a duration:%d" \
                   % (self.iface_pcap, self.pcap_file, self.duration + self.fuzz)
        print pcap_cmd

        qlen_file = get_output_file("/home/kah/cc/traces", self, "qlen", None)
        try:
            os.unlink(qlen_file)
        except OSError as e:
            if e.errno != 2:
                print "Error when deleting old qlen file: %s" % e.strerror        
        qlen_cmd = ["/home/kah/qlen.sh", self.iface_qlen, qlen_file, \
                    str(self.duration + self.fuzz)] 

        print("Starting tshark and qlen")

        term_time = time.time() + self.duration + (self.fuzz * 1.5)
        stime = time.time()
        pcap = Popen(shlex.split(pcap_cmd), stdout=PIPE, stderr=PIPE, shell=False)
        qlen = Popen(qlen_cmd, stdout=PIPE, stderr=PIPE, shell=False)

        #Give instrumentation time to start! tshark takes about 7-8s before cap!
        sys.stdout.write("Waiting 10s for instrumentation to start")
        sys.stdout.flush()
        for n in range(0,10):
            sys.stdout.write(".")
            sys.stdout.flush()
            time.sleep(1)
        print("")

        print("Executing testbed scripts")

        rcv_cmd = "ssh %s '/home/kah/%s_rcv.sh %d %s'" \
                  % (DST, self.exp, self.duration + 5, str(self.flows))
        print "rcv_cmd: ", rcv_cmd
        rcv = Popen(shlex.split(rcv_cmd), stdout=PIPE, stderr=PIPE, shell=False)
        src_cmd = "ssh %s '/home/kah/%s.sh %d %s'" \
                  % (SRC, self.exp, self.duration, str(self.flows))
        print "src_cmd: ", src_cmd
        src = Popen(shlex.split(src_cmd), stdout=PIPE, stderr=PIPE, shell=False)

        signal.signal(signal.SIGINT, self.sigint_handler)

        print("Testbed scripts executed")

        duration = timeleft(term_time)
        done = 0
        def progress():
            return 1 if done else (duration - timeleft(term_time)) / duration

        def spinner():
            s = "[" + (" " * 100) + "]" + "% 3d%%" % (0)
            s += len(s) * "\b"
            sys.stdout.write(s)
            sys.stdout.flush()
            while not done:
                time.sleep(.01)
                p = int(round(progress() * 100))
                s = "[" + ("=" * p) + (" " * (100 - p)) + "]" + "% 3d%%" % (p) 
                s += len(s) * "\b"
                sys.stdout.write(s)
                sys.stdout.flush()
            sys.stdout.write("[" + "=" * 100 + "]\n")
            sys.stdout.flush()

        print("This run will finish in %.2f seconds" % (round(timeleft(term_time))))

        #print "wait loop @ %g" % (time.time() - stime)

        t = Thread(target=spinner)
        t.start()

        self.agents = {'src': src,
                  'rcv': rcv,
                  'pcap': pcap,
                  'qlen': qlen}
        ret = {}
        try:
            for p in self.agents:
                to = timeleft(term_time)
                try:
                    ret[p] = self.agents[p].communicate(timeout=to)
                except TimeoutExpired:
                    print "Agent '%s' overran!" % p
                    self.agents[p].kill()
                    print "Post-kill wait for '%s'..." % p, 
                    ret[p] = self.agents[p].communicate(timeout=.1)
                    print " returned!"
        except OSError as e:
            print "Error while waiting for agents: %s" % str(e)
            self.agent_killer()
        finally:
            pass
        done = 1

        t.join()

        for p in ret:
            stdout, stderr = ret[p]
            print '\n****\nAGENT %s RETURNED %d' \
                % (p, self.agents[p].returncode)
            if stdout:
                print 'STDOUT:\n----\n%s\n----' % stdout
            if stderr:
                print 'STDERR:\n----\n%s----' % stderr
            
        print "\n=====\nExperiment %s-%s Run %s FINISHED" \
            % (self.exp, self.expid, self.runid)

    def run_post(self):
        pass

    def run(self, runid):
        self.runid = runid
        tracetypes = ["qlen", "pcap", "siftr", "iperf"]
        for ttype in tracetypes:
            try:
                os.makedirs(get_output_dir("/home/kah/cc/traces/", self, ttype))
            except OSError as e:
                if e.errno != errno.EEXIST:
                    print "Failed setting up tracedir: %s" % e.strerror
                    sys.exit(2)

            
        if self.run_pre():
            print "@@@@ PRE FAILED - ABORTING @@@@"
            return 1

        time.sleep(.5)
        
        if self.run_main():
            print "@@@@ MAIN FAILED - ABORTING @@@@"
            return 2

        time.sleep(.5)
        
        if self.run_post():
            print "@@@@ POST FAILED - ABORTING @@@@"
            return 3

        return 0

class RemoteSetupExperiment(Experiment):
    enable_tiu = False
    enable_fse = False
    
    def __init__(self, name, expid, flows=1, duration=10, \
                 iface_pcap=None, iface_qlen=None, fuzz=None, \
                 tiu=False, fse=False):
        Experiment.__init__(self, name, expid, flows, duration, \
                            iface_pcap, iface_qlen, fuzz)
        self.enable_tiu = tiu
        self.enable_fse = fse

    def _exec_prepost(self, host, pre, result, idx):
        stage = 'pre' if pre else 'post'

        #Need to escape the ticks so it gets to the remote shell:
        pre_cmd = r"ssh %s su root -c \'/home/kah/%s_%s.sh%s\'"

        opts = ""
        if pre:
            if self.enable_tiu:
                opts += " tiu"
            if self.enable_fse:
                opts += " fse"

        local_dir = None
        if host == SRC:
            local_dir = "../source"
        else:
            local_dir = "../sink"

        script_name = self.exp + "_" + stage + ".sh"

        if install_script(script_name, local_dir, host, "/home/kah"):
            print("Failed to install script")
            result[idx] = -1
            return

        if host == SRC:
            opts += " siftr"
            if not pre:
                opts = " %s-%s" % (self.exp, self.runid.replace("/", "-")) \
                       + opts

        print(pre_cmd % (host, self.exp, stage, opts))
        act = Popen(pre_cmd % (host, self.exp, stage, opts), \
                    stdout=PIPE, stderr=PIPE, bufsize=1, shell=True)
        q = Queue()
        t_stdout = Thread(target=enqueue_output, args=(act.stdout, None, q))
        t_stderr = Thread(target=enqueue_output, args=(act.stderr, "STDERR: ", q))
        t_stdout.start()
        t_stderr.start()

        while act.poll() == None:
            try:
                line = q.get_nowait()
                sys.stdout.write(host + ": " + line)
            except Empty:
                time.sleep(1)

        # Print residual output
        try:
            while True:
                line = q.get_nowait()
                sys.stdout.write(host + ": " + line)
        except Empty:
            pass

        t_stdout.join()
        t_stderr.join()

        if act.returncode != 0:
            print "%s on host %s FAILED (retcode %d)" \
                % (stage.upper(), host, act.returncode)
            # print "STDOUT:\n%s\nSTDERR:\n%s" % (stdout, stderr)
            result[idx] = -1
        result[idx] = 0


    def _run_prepost(self, pre=True):
        threads = []
        results = []
        idx = 0
        for host in [DST, SRC]:
            t = Thread(target=self._exec_prepost, args=(host, pre, results, idx))
            threads.append(t)
            results.append(None)
            t.start()
            idx += 1

        for thread in threads:
            thread.join()

        if -1 in results:
            return 1
        return 0

    def run_pre(self):
        print("Installing experiment scripts")
        remote_dir = "/home/kah"

        local_dir = "../source"
        script_name = "%s.sh" % (self.exp)
        if install_script(script_name, local_dir, SRC, remote_dir):
            print("Failed to install script on source")
            return -1

        local_dir = "../sink"
        script_name = "%s_rcv.sh" % (self.exp)

        if install_script(script_name, local_dir, DST, remote_dir):
            print("Failed to install script on sink")
            return -1

        print "++++ %s Run %s PRE ++++" % (self.exp, self.runid)

        return self._run_prepost(pre=True)

    def run_post(self):
        print "++++ %s Run %s POST ++++" % (self.exp, self.runid)

        ret_script = self._run_prepost(pre=False)

        if ret_script:
            return ret_script

        # scp_cmd = "scp 10.0.1.7:/var/log/siftr-%s-%s.log.xz traces/siftr/%s-%s/%s" \
        #           % (self.exp, self.runid.replace("/", "-"), self.exp, self.expid, \
        #              os.path.dirname(self.runid))
        siftr_path = get_output_dir("/home/kah/cc/traces/", self, "siftr")
        scp_cmd = "scp 10.0.1.7:/var/log/siftr-%s-%s.log.xz %s" \
                   % (self.exp, self.runid.replace("/", "-"), siftr_path)
        print scp_cmd

        try:
            check_output(shlex.split(scp_cmd))
        except:
            print "Failed to retrieve SIFTR log"
            return 1

        #Saves disk space to compress each pcap individually
        compress_file(self.pcap_file)

        return 0

class LocalSetupExperiment(RemoteSetupExperiment):
    def __init__(self, name, expid, flows=1, duration=10, \
                 iface_pcap=None, iface_qlen=None, fuzz=None, \
                 tiu=False, fse=False, rate=10e6, delay=25.0):
        RemoteSetupExperiment.__init__(self, name, expid, flows, duration, \
                                       iface_pcap, iface_qlen, fuzz, tiu, fse)

        self.rate = rate
        self.delay = delay
    
    def run_pre(self):
        shaper_cmd = "sudo /home/kah/core/do_bdp_queues.sh %d %.4f" % (self.rate, self.delay)
        try:
            check_output(shlex.split(shaper_cmd))
        except:
            print "Failed to set up bottleneck link params"
            return 1
        
        return RemoteSetupExperiment.run_pre(self)

    def run_post(self):
        ret_parent = RemoteSetupExperiment.run_post(self)

        if ret_parent:
            return ret_parent

        iperf_path = get_output_dir("/home/kah/cc/traces/", self, "iperf")
        scp_cmd = "scp 10.0.1.7:iperf-%s-%s.tar.xz %s" \
                  % (self.exp, self.runid.replace("/", "-"), iperf_path)
        print scp_cmd
        try:
            check_output(shlex.split(scp_cmd))
        except:
            print "Failed to retrieve iperf report"
            return 1

        return 0

class FixedSizeTransfer(LocalSetupExperiment):
    def __init__(self, name, flows=1, duration=10, \
                 iface_pcap=None, iface_qlen=None, fuzz=None, \
                 tiu=False, fse=False, rates=[10e6], delay=25.0, filesize=10485760):
        LocalSetupExperiment.__init__(self, name, flows, duration, \
                                      iface_pcap, iface_qlen, fuzz, \
                                      tiu, fse, rates, delay)

        self.filesize = filesize
        #HACK: We can avoid rewriting the main test runner by reusing this param:
        self.flows = filesize

class ExpRunner:
    def __init__(self, expname, expclass, configs, replicates=1, flowduration=120, xtraffic=True):
        self.expname = expname
        self.expclass = expclass
        self.configs = configs
        self.replicates = replicates
        self.fdur = flowduration
        self.crosstraffic = xtraffic
        self.flowconfigs = [1] #Override this in subclasses!
        self.tracetypes = ["pcap", "qlen", "siftr"] #Override!

        self.if_suffix = find_dev_ext()

        self.config_data = {'expname': expname,
                            'expclass': expclass.__name__,
                            'configs': configs,
                            'replicates': replicates,
                            'flow_duration': flowduration,
                            'flow_configs': self.flowconfigs}

    def instantiate_exp(self, nflows, config):
        """ Default experiment class instantiator """
        return self.expclass(self.expname, nflows)

    def run(self):
        self._run()

    def _run(self):
        self.report = []

        self.coresession = find_core_session()

        self._run_inner()

        self._run_report()

    def _run_inner(self):
        for config in self.configs:
            for nflows in self.flowconfigs:
                e = self.instantiate_exp(nflows, config)
                run = 1
                for replicate in range(self.replicates):
                    if self.crosstraffic:
                        xsource = start_crosstraffic(self.coresession, run)

                    res = e.run(config + ("/run%03d" % run))

                    if self.crosstraffic:
                        xsource.terminate()

                    self.report.append({'config': config, 'nflows':
                                   nflows, 'expid': e.expid, \
                                   'result': res, 'run': run,
                                   'duration': e.duration})

                    run += 1

    def _run_report(self):

        report = self.report
                    
        print "\n###### REPORT ######"
        good_exps = filter(lambda e: e['result'] == 0, report)
        print "%d/%d experiments OK" % (len(good_exps), len(report))

        if len(good_exps) < len(report):
            print"\nFAILURES:\nConfig\tExpId\tRun\tnFlows\tDuration\tResult\n--------"

        bad_exps = filter(lambda e: e['result'] != 0, report)
        report_print(bad_exps)

        print"\nSUCCESSES:\nConfig\tExpId\tRun\tnFlows\tDuration\tResult\n--------"
        report_print(good_exps)

    
        tartime = time.strftime("%d%m-%H%M")

        # reportname = 'traces/%s-%s.csv' % (self.expname, tartime)
        reportname = '/home/kah/cc/traces/%s-%s.csv' % (self.expname, tartime)
        with open(reportname, 'w') as reportfile:
            reportwriter = csv.DictWriter(reportfile, fieldnames=["config", "nflows", \
                                                              "expid", "run", \
                                                              "duration", "result"])
            reportwriter.writeheader()
            reportwriter.writerows(good_exps)

        #This probably got overridden by a subclass:
        self.config_data['flow_config'] = self.flowconfigs
        # configname = 'traces/%s-%s.json' % (self.expname, tartime)
        configname = '/home/kah/cc/traces/%s-%s.json' % (self.expname, tartime)
        with open(configname, 'w') as configfile:
            json.dump(self.config_data, configfile, indent=4)
        
        tarname = "%s-traces-%s.tar" % (self.expname, tartime)
        """with tarfile.open(tarname, "w:") as tar:
            for exp in good_exps:
                print "Tarring exp %s..." % exp['expid']
                for trace in self.tracetypes:
                    #Somehow this adds up # replicates times the files: FIXME
                    tar.add("traces/%s/%s-%s/%s" % (trace, self.expname, exp['expid'], exp['config']))
                    print "\tAdding trace %s" % ("traces/%s/%s-%s/%s" % (trace, self.expname, exp['expid'], exp['config']))
            tar.add(reportname)
            tar.add(configname)"""


        tar_args = [reportname, configname]
        tarred = []
        for exp in good_exps:
            if exp['expid'] in tarred:
                continue
            tarred.append(exp['expid'])
            
            print "Tarring exp %s..." % exp['expid']
            for trace in self.tracetypes:
                tar_args.append("/home/kah/cc/traces/%s/%s-%s/%s" % (trace, self.expname, exp['expid'], exp['config']))
                print "\tAdding trace %s" % ("traces/%s/%s-%s/%s" % (trace, self.expname, exp['expid'], exp['config']))

        try:
            tar_out = check_output(["tar", "-cf", tarname ] + tar_args)
            compress_file(tarname)
        except:
            print "Failed to create archive, please manually fetch traces"
            print tar_out
            sys.exit(5)

        
        print "Traces w/ report + config saved to tarball: %s.xz" % tarname

