#!/bin/sh

DURATION=$1
FILESIZE_LONG="25M"
FILESIZE_SHORT="1M"
PORT_A=5001
PORT_B=5002
B_OFFSET=5

#if [ -z $FILESIZE ] || [ $FILESIZE -lt 1 ]; then
#    echo "Must set duration and filesize!"
#    exit 1
#fi

PIDS=""

echo "Woohoo"
sleep 1

iperf -c 10.0.0.5 -p $PORT_A -n $FILESIZE_LONG -y c > cc/transfertime-$PORT_A.csv &
PIDS="$PIDS $!"

sleep $B_OFFSET
iperf -c 10.0.0.5 -p $PORT_B -n $FILESIZE_SHORT -y c > cc/transfertime-$PORT_B.csv &
PIDS="$PIDS $!"

echo $PIDS
for pid in $PIDS; do
	echo "Gonna wait for pid '$pid'"
	wait $pid
done

echo "Done"
