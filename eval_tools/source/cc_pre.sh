#!/bin/sh

TIU=0
FSE=0
SIFTR=0

echo "Executing $0"

for arg; do
    if [ "$arg" == "tiu" ]; then
	TIU=1
    fi
    if [ "$arg" == "fse" ]; then
	FSE=1
    fi
    if [ "$arg" == "siftr" ]; then
	SIFTR=1
    fi
done

# Do this immediately, as it takes forever
sysctl net.inet.tcp.hostcache.purge=1

if [ "$SIFTR" -eq 1 ]; then
    kldload siftr
fi

if [ "$FSE" -eq 1 ]; then
    sysctl net.inet.tcp.cc.algorithm=newreno_afse
else
    sysctl net.inet.tcp.cc.algorithm=newreno
fi

# sysctl net.inet.tcp.cc.fse.ack_clocking=1

if [ "$SIFTR" -eq 1 ]; then
    rm -f /var/log/siftr.log    
    sysctl net.inet.siftr.enabled=1
fi
echo "PRE request: TiU=$TIU FSE=$FSE SIFTR=$SIFTR"

mkdir -p cc/
rm -r cc/transfertime-*.csv


#Kill cached state
sysctl net.inet.tcp.cc.fse.reaper_limit=5
sysctl net.inet.tcp.cc.fse.reaper_ival=3
# sysctl net.inet.tcp.hostcache.cachelimit=0 # This is read-only...

#Reaper should have removed all FSE state during this sleep:
NAPTIME=15
echo "Sleep for FSE purge ($NAPTIME seconds)..."
sleep $NAPTIME
echo "FSE purge completed"

#Wait for hostcache to become purged
# echo "Waiting for hostcache purge... (this step takes forever -_-)"
# PURGE=1
# while [ "$PURGE" -eq 1 ]; do
# 	PURGE=`sysctl -n net.inet.tcp.hostcache.purge`
# 	sleep 3
# done
# echo "Hostcache purged"

#Reset to default, don't want interference while the test runs
sysctl net.inet.tcp.cc.fse.reaper_limit=180
sysctl net.inet.tcp.cc.fse.reaper_ival=90

echo "Finished executing $0"
