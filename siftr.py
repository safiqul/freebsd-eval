import csv
import sys

import matplotlib.pyplot as pyplot
import matplotlib.cbook as cbook
from pylab import plotfile, show, gca, sqrt
import math

import os.path

fig_width_pt = 400.0  # Get this from LaTeX using \showthe\columnwidth
inches_per_pt = 1.0/72.27               # Convert pt to inch
golden_mean = (sqrt(5)-1.0)/2.0         # Aesthetic ratio
fig_width = fig_width_pt*inches_per_pt  # width in inches
fig_height = fig_width*golden_mean      # height in inches
fig_size =  [fig_width,fig_height]

class SIFTRFile(object):

    # Column name -> column number
    column = {
        "io": 0,
        "memaddr": 1,
        "time": 2,
        "src": 3,
        "src_port": 4,
        "dst": 5,
        "dst_port": 6,
        "ssthresh": 7,
        "cwnd": 8,
    }

    # Functions to convert the data in column N
    conv = {
        2: lambda x: float(x),
        4: lambda x: int(x),
        6: lambda x: int(x),
        7: lambda x: int(x),
        8: lambda x: int(x),
    }

    def __init__(self, filename):
        self.filename = filename
        self.cwnd_limit = 5000000
        self.data = []

        self.conv = self.conv

        self.parse()

    def parse(self):
        with open(self.filename, 'r') as f:
            csv_reader = csv.reader(f)
    
            for row in csv_reader:
                if row[0].startswith("enable"):
                    # print("First line")
                    continue
                if row[0].startswith("disable"):
                    # print("Last line")
                    break

                for k in self.conv:
                    row[k] = self.conv[k](row[k])
                
                if self.cwnd_limit and row[self.column["cwnd"]] > self.cwnd_limit:
                    continue

                if self.cwnd_limit and row[self.column["ssthresh"]] > self.cwnd_limit:
                    row[self.column["ssthresh"]] = 0

                self.data.append(row)

    def filter(self, src=None, src_port=None, dst=None, dst_port=None):
        if len(self.data) == 0:
            print("NO DATA")

        column = self.column
        # Iterate all lines
        for row in self.data:
            # Filter rows
            if src_port and row[column["src_port"]] != src_port:
                continue
            if dst_port and row[column["dst_port"]] != dst_port:
                continue
            if src and row[column["src"]] != src:
                continue
            if dst and row[column["dst"]] != dst:
                continue
            yield row
            
    def completion_time(self, src=None, src_port=None, dst=None, dst_port=None):
        column = self.column
        first = None

        # Iterate all lines
        for row in self.filter(src, src_port, dst, dst_port):
            # Store the first matching row
            if not first:
                first = row
            # Store the last encountered matching row
            last = row

        if first:
            delta = last[column["time"]] - first[column["time"]]
            # print("FCT:", delta)
            return delta
        else:
            # print("No matching rows")
            return None


    # time_shift = True => start x at 0
    def cwnd(self, src=None, src_port=None, dst=None, dst_port=None, time_shift=True, time_offset=0.0):
        column = self.column
        x = []
        y = []
        offset = 0.0

        for row in self.filter(src, src_port, dst, dst_port):
            if time_shift:
                time_shift = False
                offset = row[column["time"]]
            x.append(time_offset + row[column["time"]] - offset)
            y.append(row[column["cwnd"]])
        return x,y

    def ssthresh(self, src=None, src_port=None, dst=None, dst_port=None, time_shift=True):
        column = self.column
        x = []
        y = []
        offset = 0.0

        for row in self.filter(src, src_port, dst, dst_port):
            if time_shift:
                time_shift = False
                offset = row[column["time"]]

            # Skip entries with "undefined" values of sshthresh
            if len(x) == 0 and (row[column["ssthresh"]] == 0 or row[column["ssthresh"]] > 5000000):
                continue

            x.append(row[column["time"]] - offset)
            y.append(row[column["ssthresh"]])
        return x,y

class Plot(object):
    def __init__(self):
        self.fig = pyplot.figure(1)
        self.splot = self.fig.add_subplot(111)
        self.colors = ['blue', 'red', 'green', 'orange', 'black', 'pink']

        pass

    def title(self, title):
        # self.splot.title(title) 
        pass

    def labels(self, xlabel, ylabel):
        self.splot.set_xlabel(xlabel)
        self.splot.set_ylabel(ylabel)

    def plot(self, x, y, labl, color=None):
        self.splot.plot(x, y, label=labl, color=color if color else self.colors.pop(0), linewidth=1)

    def save(self, filename):
        self.splot.legend(loc='lower right', shadow=True)
        self.splot.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=5, mode="expand", borderaxespad=0.)
        self.splot.grid(True, which="both")

        self.fig.savefig(filename, dpi=300, bbox_inches='tight')

