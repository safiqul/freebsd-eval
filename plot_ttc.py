import sys
from siftr import *
import csv
import os.path

import numpy as np

if __name__ == '__main__':
    args = sys.argv[1:]

    root = args[0]
    report = os.path.join(root, "report.csv")

    runs = []
    with open(report, 'r') as reportfile:
        reader = csv.DictReader(reportfile)
        runs = [row for row in reader]

    data = {}

    for run in runs:
        print("Run: %s, conf: %s, rate: %s" % (run["run"], run["config"], run["rate"]))
        config = run["config"]
        rate = int(float(run["rate"]))

        if not rate in data:
            data[rate] = {}

        if not config in data[rate]:
            data[rate][config] = {"long": [], "short": []}

        s = SIFTRFile(os.path.join(root, "siftr/siftr-cc-%s-run%03d.log" % (run["config"], int(run["run"]))))
        long_fct = s.completion_time(dst_port=5001, src="10.0.1.7", dst="10.0.0.5")
        short_fct = s.completion_time(dst_port=5002, src="10.0.1.7", dst="10.0.0.5")
        data[rate][config]["long"].append(long_fct)
        data[rate][config]["short"].append(short_fct)

    print(data)

    # args = sys.argv[1:]

    # s = SIFTRFile(args[0])

    # p = Plot()
    # p.title("CWND")
    # p.labels("Time (s)", "bytes")

    # x,y = s.cwnd(dst_port=5001, src="10.0.1.7", dst="10.0.0.5")
    # p.plot(x,y, "cwnd")

    # x,y = s.ssthresh(dst_port=5001, src="10.0.1.7", dst="10.0.0.5")
    # p.plot(x,y, "ssthresh")

    # p.save(args[1])

