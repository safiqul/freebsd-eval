#! /usr/bin/env bash

echo "1: $1"

if [[ "$1" == "" ]]; then
    LATEST=`find traces -mindepth 1 -maxdepth 1 -type d | sort -r | head -1`
    EXP=`echo $LATEST | cut --delim='/' -f 2`
    OUTPUT_DIR=graphs/$EXP
else
    EXP=`echo $1 | cut --delim='/' -f 2`
    OUTPUT_DIR=graphs/$EXP
fi

# Create output dir
mkdir -p $OUTPUT_DIR

# Uncompress all files
find ./traces/$EXP -name "*.log.xz" -exec unxz \{\} \;

# Plot cwnd for each run
for f in `ls ./traces/$EXP/siftr/`; do
    input=./traces/$EXP/siftr/$f
    if [[ $input == *".xz" ]]; then
        echo "Decompressing..."
        unxz $input
        input=./traces/$EXP/siftr/`basename -s ".xz" $input`
        echo "Decompressed $input"
    fi

    basename=`basename -s ".log" $input`
    newname=`echo "$basename" | cut --delim="-" -f 3-`
    output=$OUTPUT_DIR/cwnd-$newname.png

    echo "Plotting cwnd for $input"
    python3 plot_cwnd.py "$input" "$output"
    echo "Written to $output"
done
